
/***************************************************************************
 *   Copyright (C) $Year$ by $Author: e1100983 $   *
 *   $Email$   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*********************************************************************
*
* 1.  NAME
*     $Source$ 
* 2.  DESCRIPTION
*
* 6.  VERSIONS
*       Original:
*         $Date: 2014-03-29 17:41:28 +0200 (Sat, 29 Mar 2014) $ / $Author: e1100983 $
*
*       Version history: Veni , Vidi , Vici
*        
*
**********************************************************************/

/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h"

/*********************************************************************
*    FUNCTIONS                                                     *
**********************************************************************/
/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME: draw_board
; DESCRIPTION: draws gameboard to screen with ncurses
;	Input: -
;	Output: gameboard
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/
void draw_board() {
	int x = 1;
	int i = 2;

/* axis coordinates */
	for(x; x<=BOARD_SIZE*2; x=x+2) {
		move(0,x);
		printw("%d",x/2);
		move(0,x+RIGHT_ALIGN);
		printw("%d",x/2);
		move(x,0);
		printw("%d",x/2);
		move(x,RIGHT_ALIGN);
		printw("%d",x/2);
	} /* end of for*/
	
/* draw lines */
	for(i;i<BOARD_SIZE*2;i=i+2) {
		x = 1;
		for(x;x<BOARD_SIZE*2;x++) {
			move(i,x);
			if((x%2)!=0) addch(ACS_HLINE);
			if((x%2)==0) addch(ACS_PLUS);
			move(i,x+RIGHT_ALIGN);
			if((x%2)!=0) addch(ACS_HLINE);
			if((x%2)==0) addch(ACS_PLUS);
			move(x,i);
			if((x%2)!=0) addch(ACS_VLINE);
			move(x,i+RIGHT_ALIGN);
			if((x%2)!=0) addch(ACS_VLINE);
		} /* end of for */
	} /* end of for */
	refresh();
	move(BOARD_SIZE*2+2,0);
} /* end of draw_board */
