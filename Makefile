SHIP_SHRINK:	add.o ask_integer.o compare.o draw_board.o draw_boat.o file.o header.h main.o shoot.o game.o
	gcc -o SHIP_SHRINK add.o ask_integer.o compare.o draw_board.o draw_boat.o file.o main.o shoot.o game.o -lncurses -std=c99
add.o:	add.c header.h
	gcc -c add.c
ask_integer.o:	ask_integer.c header.h
	gcc -c ask_integer.c
compare.o:	compare.c header.h
	gcc -c compare.c -std=c99
draw_board.o:	draw_board.c header.h
	gcc -c draw_board.c
draw_boat.o:	draw_boat.c header.h
	gcc -c draw_boat.c
file.o:	file.c header.h
	gcc -c file.c -std=c99
main.o:	main.c header.h
	gcc -c main.c
shoot.o:	shoot.c header.h
	gcc -c shoot.c
game.o:	game.c header.h
	gcc -c game.c
clean:
	rm *.o SHIP_SHRINK
