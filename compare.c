/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h"

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:compare
; DESCRIPTION:tuli pari muuttujaa 
;	Input:
;	Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

//\n1 to right\n2 to down\n3 to left\n4 to up\n



void compare(int plr ,int x_coord ,int y_coord)		{
fileout(plr);
int j;
int i = 1;

if(plr == 0)		{
	j = 1;	
}
else			{
	j = 0;
}
struct boats *ptr;
ptr = (struct boats*)&player[j];
int x1,x2,y1,y2;
		//CARRIER
switch(ptr->_carrier[CARRIER][2])		{
	case 1:
		x1 = ptr->_carrier[CARRIER][0];
		x2 = ptr->_carrier[CARRIER][0] + CARRIER;
		y1 = ptr->_carrier[CARRIER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_carrier[CARRIER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_carrier[CARRIER][0];
		y1 = ptr->_carrier[CARRIER][1]; 
		y2 = ptr->_carrier[CARRIER][1] + CARRIER;
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_carrier[CARRIER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_carrier[CARRIER][0];
		x1 = ptr->_carrier[CARRIER][0] + CARRIER;
		y1 = ptr->_carrier[CARRIER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_carrier[CARRIER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_carrier[CARRIER][0];
		y2 = ptr->_carrier[CARRIER][1]; 
		y1 = ptr->_carrier[CARRIER][1] + CARRIER;
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_carrier[CARRIER][3] = TRUE;
				break; 
			}
	}
		//BATTLESHIP1	
switch(ptr->_battleship1[BATTLESHIP][2])		{
	case 1:
		x1 = ptr->_battleship1[BATTLESHIP][0];
		x2 = ptr->_battleship1[BATTLESHIP][0] + BATTLESHIP;
		y1 = ptr->_battleship1[BATTLESHIP][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_battleship1[BATTLESHIP][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_battleship1[BATTLESHIP][0];
		y1 = ptr->_battleship1[BATTLESHIP][0] + BATTLESHIP;
		y2 = ptr->_battleship1[BATTLESHIP][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_battleship1[BATTLESHIP][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_battleship1[BATTLESHIP][0];
		x1 = ptr->_battleship1[BATTLESHIP][0] + BATTLESHIP;
		y1 = ptr->_battleship1[BATTLESHIP][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_battleship1[BATTLESHIP][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_battleship1[BATTLESHIP][0];
		x2 = ptr->_battleship1[BATTLESHIP][0] + BATTLESHIP;
		y1 = ptr->_battleship1[BATTLESHIP][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_battleship1[BATTLESHIP][3] = TRUE;
				break; 
			}
	}
		//BATTLESHIP2
switch(ptr->_battleship2[BATTLESHIP][2])		{
	case 1:
		x1 = ptr->_battleship2[BATTLESHIP][0];
		x2 = ptr->_battleship2[BATTLESHIP][0] + BATTLESHIP;
		y1 = ptr->_battleship2[BATTLESHIP][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_battleship2[BATTLESHIP][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_battleship2[BATTLESHIP][0];
		y1 = ptr->_battleship2[BATTLESHIP][0] + BATTLESHIP;
		y2 = ptr->_battleship2[BATTLESHIP][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_battleship2[BATTLESHIP][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_battleship2[BATTLESHIP][0];
		x1 = ptr->_battleship2[BATTLESHIP][0] + BATTLESHIP;
		y1 = ptr->_battleship2[BATTLESHIP][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_battleship2[BATTLESHIP][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_battleship2[BATTLESHIP][0];
		x2 = ptr->_battleship2[BATTLESHIP][0] + BATTLESHIP;
		y1 = ptr->_battleship2[BATTLESHIP][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_battleship2[BATTLESHIP][3] = TRUE;
				break; 
			}
	}
		//CRUISER1
switch(ptr->_cruiser1[CRUISER][2])		{
	case 1:
		x1 = ptr->_cruiser1[CRUISER][0];
		x2 = ptr->_cruiser1[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser1[CRUISER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_cruiser1[CRUISER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_cruiser1[CRUISER][0];
		y1 = ptr->_cruiser1[CRUISER][0] + CRUISER;
		y2 = ptr->_cruiser1[CRUISER][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_cruiser1[CRUISER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_cruiser1[CRUISER][0];
		x1 = ptr->_cruiser1[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser1[CRUISER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_cruiser1[CRUISER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_cruiser1[CRUISER][0];
		x2 = ptr->_cruiser1[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser1[CRUISER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_cruiser1[CRUISER][3] = TRUE;
				break; 
			}
	}
		//CRUISER2
switch(ptr->_cruiser2[CRUISER][2])		{
	case 1:
		x1 = ptr->_cruiser2[CRUISER][0];
		x2 = ptr->_cruiser2[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser2[CRUISER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_cruiser2[CRUISER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_cruiser2[CRUISER][0];
		y1 = ptr->_cruiser2[CRUISER][0] + CRUISER;
		y2 = ptr->_cruiser2[CRUISER][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_cruiser2[CRUISER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_cruiser2[CRUISER][0];
		x1 = ptr->_cruiser2[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser2[CRUISER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_cruiser2[CRUISER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_cruiser2[CRUISER][0];
		x2 = ptr->_cruiser2[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser2[CRUISER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_cruiser2[CRUISER][3] = TRUE;
				break; 
			}
	}
		//CRUISER3
switch(ptr->_cruiser2[CRUISER][2])		{
	case 1:
		x1 = ptr->_cruiser3[CRUISER][0];
		x2 = ptr->_cruiser3[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser3[CRUISER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_cruiser3[CRUISER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_cruiser3[CRUISER][0];
		y1 = ptr->_cruiser3[CRUISER][0] + CRUISER;
		y2 = ptr->_cruiser3[CRUISER][1]; 
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_cruiser3[CRUISER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_cruiser3[CRUISER][0];
		x1 = ptr->_cruiser3[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser3[CRUISER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_cruiser3[CRUISER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_cruiser3[CRUISER][0];
		x2 = ptr->_cruiser3[CRUISER][0] + CRUISER;
		y1 = ptr->_cruiser3[CRUISER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_cruiser3[CRUISER][3] = TRUE;
				break; 
			}
	}
		//DESTROYER1
switch(ptr->_destroyer1[DESTROYER][2])		{
	case 1:
		x1 = ptr->_destroyer1[DESTROYER][0];
		x2 = ptr->_destroyer1[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer1[DESTROYER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer1[DESTROYER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_destroyer1[DESTROYER][0];
		y1 = ptr->_destroyer1[DESTROYER][0] + DESTROYER;
		y2 = ptr->_destroyer1[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer1[DESTROYER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_destroyer1[DESTROYER][0];
		x1 = ptr->_destroyer1[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer1[DESTROYER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer1[DESTROYER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_destroyer1[DESTROYER][0];
		x2 = ptr->_destroyer1[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer1[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer1[DESTROYER][3] = TRUE;
				break; 
			}
	}
		//DESTROYER2
switch(ptr->_destroyer2[DESTROYER][2])		{
	case 1:
		x1 = ptr->_destroyer2[DESTROYER][0];
		x2 = ptr->_destroyer2[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer2[DESTROYER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer2[DESTROYER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_destroyer2[DESTROYER][0];
		y1 = ptr->_destroyer2[DESTROYER][0] + DESTROYER;
		y2 = ptr->_destroyer2[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer2[DESTROYER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_destroyer2[DESTROYER][0];
		x1 = ptr->_destroyer2[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer2[DESTROYER][1];
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer2[DESTROYER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_destroyer2[DESTROYER][0];
		x2 = ptr->_destroyer2[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer2[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer2[DESTROYER][3] = TRUE;
				break; 
			}
	}
		//DESTROYER3
switch(ptr->_destroyer3[DESTROYER][2])		{
	case 1:
		x1 = ptr->_destroyer3[DESTROYER][0];
		x2 = ptr->_destroyer3[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer3[DESTROYER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer3[DESTROYER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_destroyer3[DESTROYER][0];
		y1 = ptr->_destroyer3[DESTROYER][0] + DESTROYER;
		y2 = ptr->_destroyer3[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer3[DESTROYER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_destroyer3[DESTROYER][0];
		x1 = ptr->_destroyer3[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer3[DESTROYER][1];
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer3[DESTROYER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_destroyer3[DESTROYER][0];
		x2 = ptr->_destroyer3[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer3[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer3[DESTROYER][3] = TRUE;
				break; 
			}
	}
		//DESTROYER4
switch(ptr->_destroyer4[DESTROYER][2])		{
	case 1:
		x1 = ptr->_destroyer4[DESTROYER][0];
		x2 = ptr->_destroyer4[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer4[DESTROYER][1]; 
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer4[DESTROYER][3] = TRUE;
				break; 
			}
	case 2: 
		x1 = ptr->_destroyer4[DESTROYER][0];
		y1 = ptr->_destroyer4[DESTROYER][0] + DESTROYER;
		y2 = ptr->_destroyer4[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer3[DESTROYER][3] = TRUE;
				break; 
			}	
	case 3:
		x2 = ptr->_destroyer4[DESTROYER][0];
		x1 = ptr->_destroyer4[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer4[DESTROYER][1];
		if(x_coord >= x1  && x_coord <= x2 && y1 == y_coord)	{
				ptr->_destroyer4[DESTROYER][3] = TRUE;
				break; 
			}
	case 4:
		x1 = ptr->_destroyer4[DESTROYER][0];
		x2 = ptr->_destroyer4[DESTROYER][0] + DESTROYER;
		y1 = ptr->_destroyer4[DESTROYER][1];
		if(y_coord >= y1  && y_coord <= y2 && x1 == x_coord)	{
				ptr->_destroyer4[DESTROYER][3] = TRUE;
				break; 
			}
			
	}

}
