/***************************************************************************
 *   Copyright (C) $Year$ by $Author: e1100983 $   *
 *   $Email$   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*********************************************************************
*
* 1.  NAME
*     $Source$
* 2.  DESCRIPTION
*
* 6.  VERSIONS
*       Original:
*         $Date: 2014-03-30 00:09:49 +0200 (Sun, 30 Mar 2014) $ / $Author: e1100983 $
*
*       Version history: Veni , Vidi , Vici
*
*
**********************************************************************/

/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include <ncurses.h>	/* ncurses.h includes stdio.h */ 
/*-------------------------------------------------------------------*
*    GLOBAL VARIABLES                                                *
*--------------------------------------------------------------------*/
/* Control flags */
#define DEBUG

/* Globaal constants */
/*! \def Input from keyboard, small y*/
#define YES 121
/*! \def Input from keyboard, small n*/
#define NO 110
/*! \def Board size*/
#define BOARD_SIZE 10
/*! \def Parameter to align second drawn board*/
#define RIGHT_ALIGN 30
/*! \def Parameter for Carrier length*/
#define CARRIER 5
/*! \def Parameter for Battleship length*/
#define BATTLESHIP 4
/*! \def Parameter for Cruiser length*/
#define CRUISER 3
/*! \def Parameter for Destroyer length*/
#define DESTROYER 2
/*! \def Parameter for enter key ascii code*/
#define ENTER 10
/*! \def Parameter for escape key ascii code*/
#define ESC 27
/*! \def Parameter for backspace key ascii code*/
#define BACKSPACE 8
/*! \def Parameter for minimum shot input, 48(ascii) is 0 on keyboard.*/
#define SHOT_MIN 0
/*! \def Parameter for maximum shot input, 57(ascii) is 9 on keyboard.*/
#define SHOT_MAX 9

/* Global macros */
/*! \def Macro for player[plr]*/
#define pllr player[plr]

/* Global variables */

/* Global structures */
/*! @struct boats
	@brief Global struct "boats", with information for input ships.*/
struct boats {
	int _carrier[CARRIER][4];
	int _battleship1[BATTLESHIP][4];
	int _battleship2[BATTLESHIP][4];
	int _cruiser1[CRUISER][4];
	int _cruiser2[CRUISER][4];
	int _cruiser3[CRUISER][4];
	int _destroyer1[DESTROYER][4];
	int _destroyer2[DESTROYER][4];
	int _destroyer3[DESTROYER][4];
	int _destroyer4[DESTROYER][4];
	int shots[10][10];
} player[2];
/*-------------------------------------------------------------------*
*    FUNCTION PROTOTYPES                                             *
*--------------------------------------------------------------------*/
/*! @brief Saves struct player[#] to file file#.txt, where # is player number.*/
void filein(int plr);
/*! @brief Reads values for struct "player[#] from file#.txt, where # is player number.*/
void fileout(int plr);
/*! @brief Compares shot coordinates to opposing player boat coordinates. */
void compare(int plr, int x_coord , int y_coord );
/*! @brief Draws gameboard with ncurses.*/
void draw_board();
/*! @brief Sets player number.*/
void set_name();
/*! @brief Adds boats by for-looping "add_boat" function.*/
void add(int plr);
/*! @brief Adds a single boat.*/
void add_boat(int plr, int ship, int size);
/*! @brief Asks for integer.*/
int ask_integer(int min, int max);
/*! @brief Adds boats to struct.*/
void add_boat_struct(int ship, int direction, int size, int plr);
/*! @brief Main-like function which runs the game itself, launched from menu.*/
void game(int plr);
/*! @brief Asks for shot coordinates*/
void shoot(int player_number);
/*! @brief Initializes ncurses with necessary parameters.*/
void init_ncurses();
/*! @brief Draws a box in the middle of the window with menu inside.*/
int draw_menubox(WINDOW *menubox,long row,long col);
/*! @brief Paints the default appearance of the menu in menubox.*/
void paint_normal(WINDOW *menunox);
/*! @brief Prints the game logo at the top side of the window while in menu.*/
void draw_logo(long row,long col);
/*! @brief Prints the babe ascii art at the bottom of the window while in menu.*/
void draw_babe(long row,long col);
/*! @brief Prints credits when enter is pressed at "Credits" in menu.*/
void draw_credits(WINDOW *menubox);
/*! @brief Main-like function which runs the game itself, launched from menu.*/
void game(int plr);
/*! @brief Asks for shot coordinates*/
void shoot(int player_number);
/*! @brief Initializes ncurses with necessary parameters.*/
void init_ncurses();
/*! @brief Draws a box in the middle of the window with menu inside.*/
int draw_menubox(WINDOW *menubox,long row,long col);
/*! @brief Paints the default appearance of the menu in menubox.*/
void paint_normal(WINDOW *menunox);
/*! @brief Prints the game logo at the top side of the window while in menu.*/
void draw_logo(long row,long col);
/*! @brief Prints the babe ascii art at the bottom of the window while in menu.*/
void draw_babe(long row,long col);
/*! @brief Prints credits when enter is pressed at "Credits" in menu.*/
void draw_credits(WINDOW *menubox);

void draw_shotted(int plr);

void filecheck(int plr);
/*********************************************************************/
