/***************************************************************************
 *   Copyright (C) $Year$ by $Author: e1100983 $   *
 *   $Email$   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*********************************************************************
*
* 1.  NAME shoot.c
*     $Source$ 
* 2.  DESCRIPTION
*
* 6.  VERSIONS
*       Original:
*         $Date: 2014-03-30 00:09:49 +0200 (Sun, 30 Mar 2014) $ / $Author: e1100983 $
*
*       Version history: Veni , Vidi , Vici
*        
*
**********************************************************************/

/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h"

/*********************************************************************
*    FUNCTIONS                                                     *
**********************************************************************/
/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:shoot.c
; DESCRIPTION:calls for compare function with shot coordinates
;	Input:none
;	Output:none
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

void shoot(int player_number)
{
int x_coord,y_coord;
int exit=0;
int i=1;
do {

	switch(i){

		case 1:
		printw("Press x coordinate for shot(0-9):");
		x_coord=getch();
		x_coord=x_coord-48;
		if(x_coord>=SHOT_MIN && x_coord<=SHOT_MAX){
			i=2;
		}
		break;

		case 2:
		printw("\nPress y coordinate for shot(0-9):");
		y_coord=getch();
		y_coord=y_coord-48;
		if(y_coord>=SHOT_MIN && y_coord<=SHOT_MAX){
			i=3;
		}
		break;

		case 3:
		printw("\nPlease verify shot coordinates: x=%d, y=%d.\nPress 'Y' to verify or 'N' to reset.",x_coord,y_coord);
		exit=getch();
		if(exit==NO){
			printw("\n");
			i=1;
		}
		if(exit==YES){
			exit=1;
		}
		if(exit!=NO && exit!=YES){
			i=3;
		}
		break;

	}
}while(exit!=1);
player[player_number].shots[y_coord][x_coord]=1;
compare(player_number, x_coord, y_coord);
} /* end of shoot*/


