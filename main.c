/***************************************************************************
 *   Copyright (C) 2014 by Atte Vainionpää				   *
 *   atte.vainionpaa@gmail.com						   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*********************************************************************
*
* 1.  NAME
*     menu.c
* 2.  DESCRIPTION
*     menu for laivanupotus
* 6.  VERSIONS
*       Original:
*         $Date: 2014-03-29 19:48:14 +0200 (Sat, 29 Mar 2014) $ / $Author: e1100983 $
*
*       Version history: Veni , Vidi , Vici
*
*
**********************************************************************/

/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h"

/*-------------------------------------------------------------------*
*    GLOBAL VARIABLES                                                *
*--------------------------------------------------------------------*/
/* Control flags */
#define DEBUG
/* Global constants */
/* Global variables */
/* Global structures */

/*-------------------------------------------------------------------*
*    FUNCTION PROTOTYPES                                             *
*--------------------------------------------------------------------*/
//void draw_credits(long row,long col);
/*********************************************************************
*    MAIN PROGRAM                                                    *
**********************************************************************/

int main(void){
	WINDOW *menubox;
	init_ncurses();
	long row,col;
	getmaxyx(stdscr,row,col);
	bkgd(COLOR_PAIR(2));
	draw_logo(row,col);
	draw_babe(row,col);
	menubox=subwin(stdscr,7,20,(row/2)-9,(col-20)/2);

	draw_menubox(menubox,row,col);

 	delwin(menubox);
	endwin();
    	return 0;
} /* end of main */

/*********************************************************************
*    FUNCTIONS                                                       *
**********************************************************************/

/*********************************************************************
;       F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:init_ncurses
; DESCRIPTION:initializes ncurses for use
;       Input:
;       Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/
void init_ncurses() {

	initscr();
	clear();
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_CYAN);
	init_pair(2, COLOR_CYAN, COLOR_BLACK);
	init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(4, COLOR_GREEN, COLOR_BLACK);
	init_pair(5, COLOR_MAGENTA, COLOR_CYAN);
	init_pair(6, COLOR_YELLOW, COLOR_BLACK);
	init_pair(7, COLOR_RED, COLOR_BLACK);
	curs_set(0);
	noecho();
	keypad(stdscr,TRUE);
}/*end of init_ncurses*/

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
COLOR_PAIR; NAME:draw_menu
; DESCRIPTION:draws a menubox in the center of the screen
;	Input:WINDOW *menubox, row, col
;	Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/
int draw_menubox(WINDOW *menubox,long row,long col){
	int key;
	int i =1;
	int x =0;
	refresh();
	wbkgd(menubox,COLOR_PAIR(2));
	paint_normal(menubox);

	do {
		if (key==KEY_DOWN && (i <= 3 || i >= 1)) {
		i++;
		}

		if (key==KEY_UP && (i <= 3 || i >= 1)) {
		i--;
		}

		if (i==0) i=1;
		if (i==4) i=3;

		switch (i) {

			case 1:
			paint_normal(menubox);
			wmove(menubox,2,0);
			wattron(menubox,COLOR_PAIR(5));
       			waddstr(menubox,"1.");
       			wattroff(menubox,COLOR_PAIR(5));
       			wattron(menubox,COLOR_PAIR(1));
       			waddstr(menubox," Play Game");
       			wattroff(menubox,COLOR_PAIR(1));
			wrefresh(menubox);
			break;

			case 2:
			paint_normal(menubox);
			wmove(menubox,4,0);
			wattron(menubox,COLOR_PAIR(5));
       			waddstr(menubox,"2.");
       			wattroff(menubox,COLOR_PAIR(5));
       			wattron(menubox,COLOR_PAIR(1));
       			waddstr(menubox," Credits");
       			wattroff(menubox,COLOR_PAIR(1));
			wrefresh(menubox);
			break;

			case 3:
			paint_normal(menubox);
			wmove(menubox,6,0);
			wattron(menubox,COLOR_PAIR(5));
	       		waddstr(menubox,"3.");
	       		wattroff(menubox,COLOR_PAIR(5));
	       		wattron(menubox,COLOR_PAIR(1));
	       		waddstr(menubox," Exit Game");
	       		wattroff(menubox,COLOR_PAIR(1));
			wrefresh(menubox);
			break;

			default:
			move(1,1);
			printw("Virhe");
			refresh();
			break;
		}/*end of switch-case*/

		key=getch();
		if (i==3 && key==ENTER)key=ESC;

		if (i==2 && key==ENTER)draw_credits(menubox);

		if (i==1 && key==ENTER)set_name();
	}while(key!=ESC);/*end of while*/
}/*end of draw_menubox*/
/*********************************************************************
;       F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:paint normal
; DESCRIPTION:paints the menubox with normal color, no highlight
;       Input:WINDOW *menubox
;       Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

void paint_normal(WINDOW *menubox) {
	wmove(menubox,0,0);
	wattron(menubox,COLOR_PAIR(3));
	waddstr(menubox,"Laivanupotus");
	wattroff(menubox,COLOR_PAIR(3));
	wmove(menubox,2,0);
	wattron(menubox,COLOR_PAIR(3));
	waddstr(menubox,"1.");
	wattroff(menubox,COLOR_PAIR(3));
	waddstr(menubox," Play Game");
	wmove(menubox,4,0);
	wattron(menubox,COLOR_PAIR(3));
	waddstr(menubox,"2.");
	wattroff(menubox,COLOR_PAIR(3));
        waddstr(menubox," Credits");
	wmove(menubox,6,0);
	wattron(menubox,COLOR_PAIR(3));
	waddstr(menubox,"3.");
	wattroff(menubox,COLOR_PAIR(3));
        waddstr(menubox," Exit Game");
	wrefresh(menubox);
}/*end of paint_normal*/
/*********************************************************************
;       F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:draw_babe
; DESCRIPTION:prints a ASCII picture of a babe in the menu
;       Input:row,col
;       Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

void draw_babe(long row,long col){
	move((row/2),(col-64)/2);
	addstr("                                  .::::.");
	move((row/2)+1,(col-64)/2);
	addstr("                                .::::::::.");
	move((row/2)+2,(col-64)/2);
	addstr("                                :::::::::::");
	move((row/2)+3,(col-64)/2);
	addstr("                                ':::::::::::..");
	move((row/2)+4,(col-64)/2);
	addstr("                                 :::::::::::::::'");
	move((row/2)+5,(col-64)/2);
	addstr("                                 ':::::::::::.");
	move((row/2)+6,(col-64)/2);
	addstr("                                   .::::::::::::::'");
	move((row/2)+7,(col-64)/2);
	addstr("                                  .:::::::::::...");
	move((row/2)+8,(col-64)/2);
	attron(COLOR_PAIR(3));
	addstr("                                 :");
	attroff(COLOR_PAIR(3));
	addstr(":::::::::::::''");
	move((row/2)+9,(col-64)/2);
	addstr("                      .:::.       '::::::::''::::");
	move((row/2)+10,(col-64)/2);
	addstr("                    .::::::::.      ':::::'  '::::");
	move((row/2)+11,(col-64)/2);
	addstr("                  .::::':::::::.    :::::    '::::.");
	move((row/2)+12,(col-64)/2);
	addstr("                .:::::' ':::::::::. :::::      ':::.");
	move((row/2)+13,(col-64)/2);
	addstr("              .:::::'     ':::::::::.:::::       '::.");
	move((row/2)+14,(col-64)/2);
	addstr("            .::::''         '::::::::::::::       '::.");
	move((row/2)+15,(col-64)/2);
	addstr("           .::''              '::::::::::::         :::... ");
	move((row/2)+16,(col-64)/2);
	addstr("        ..::::                  ':::::::::'        .:' ''''");
	move((row/2)+17,(col-64)/2);
	addstr("     ..''''':'                    ':::::.'");
}/*end of draw_babe*/
/*********************************************************************
;       F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:draw_logo
; DESCRIPTION:draws a logo in the menu
;       Input:row,col
;       Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

void draw_logo(long row,long col){
	move((row/10),(col-64)/2);
	addstr(" ____   ___    _  _____ ____    ");
	attron(COLOR_PAIR(3));
	addstr("_   _");
	attroff(COLOR_PAIR(3));
	addstr("   _   _  ___  _____ ____");
	move((row/10)+1,(col-64)/2);
	addstr("| __ ) / _ \\  / \\|_   _/ ___| ");
	attron(COLOR_PAIR(3));
	addstr(" | \\ | |");
	attroff(COLOR_PAIR(3));
	addstr(" | | | |/ _ \\| ____/ ___|");
	move((row/10)+2,(col-64)/2);
	addstr("|  _ \\| | | |/ _ \\ | | \\___ \\  ");
	attron(COLOR_PAIR(3));
	addstr("|  \\| |");
	attroff(COLOR_PAIR(3));
	addstr(" | |_| | | | |  _| \\___ \\");
	move((row/10)+3,(col-64)/2);
	addstr("| |_) | |_| / ___ \\| |  ___) | ");
	attron(COLOR_PAIR(3));
	addstr("| |\\  |");
	attroff(COLOR_PAIR(3));
	addstr(" |  _  | |_| | |___ ___) |");
	move((row/10)+4,(col-64)/2);
	addstr("|____/ \\___/_/   \\_\\_| |____/  ");
	attron(COLOR_PAIR(3));
	addstr("|_| \\_|");
	attroff(COLOR_PAIR(3));
	addstr(" |_| |_|\\___/|_____|____/");
}/*end of draw_logo*/
/*********************************************************************
;       F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:draw_credits
; DESCRIPTION:clears ncurses screen and draws credits
;       Input:row,col
;       Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

void draw_credits(WINDOW *menubox) {
	int key=0;
	wclear(menubox);
	wmove(menubox,0,0);
	wattron(menubox,COLOR_PAIR(3));
	waddstr(menubox,"Tekijät:");
	wattroff(menubox,COLOR_PAIR(3));
	wmove(menubox,2,0);
	wattron(menubox,COLOR_PAIR(4));
	waddstr(menubox,"Tomi Knuuttila");
	wattroff(menubox,COLOR_PAIR(4));
	wmove(menubox,3,0);
	wattron(menubox,COLOR_PAIR(6));
        waddstr(menubox,"Matti Suominen");
	wattroff(menubox,COLOR_PAIR(6));
	wmove(menubox,4,0);
	wattron(menubox,COLOR_PAIR(7));
        waddstr(menubox,"Atte Vainionpää");
	wattroff(menubox,COLOR_PAIR(7));
	wmove(menubox,5,0);
	wattron(menubox,COLOR_PAIR(3));
	waddstr(menubox,"Press any key");
	wmove(menubox,6,0);
	waddstr(menubox,"to continue.");
	wattroff(menubox,COLOR_PAIR(3));
	wrefresh(menubox);
	key=getch();
	wclear(menubox);
}

