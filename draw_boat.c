
/***************************************************************************
 *   Copyright (C) $Year$ by $Author: e1100983 $   *
 *   $Email$   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*********************************************************************
*
* 1.  NAME
*     $Source$ 
* 2.  DESCRIPTION
*		draw boats to screen with ncurses
* 6.  VERSIONS
*       Original:
*         $Date: 2014-03-29 22:54:26 +0200 (Sat, 29 Mar 2014) $ / $Author: e1100983 $
*
*       Version history: Veni , Vidi , Vici
*        
*
**********************************************************************/

/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h"
/*********************************************************************
*    FUNCTIONS                                                     *
**********************************************************************/
/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:	void draw_boat
; DESCRIPTION: draw boats to screen with ncurses
;	Input: int ship, int plr -- int ship == 11 for draw all boats for player
;	Output: boat positions
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/
void draw_boat(int ship, int plr) {
	int i;
	if(ship>1){
		/* ship 1 */
		for(i=0;i<CARRIER;i++){
			mvprintw(pllr._carrier[i][0]*2+1,pllr._carrier[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>2){
		/* ship 1 */
		for(i=0;i<BATTLESHIP;i++){
			mvprintw(pllr._battleship1[i][0]*2+1,pllr._battleship1[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>3){
		/* ship 1 */
		for(i=0;i<BATTLESHIP;i++){
			mvprintw(pllr._battleship2[i][0]*2+1,pllr._battleship2[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>4){
		/* ship 1 */
		for(i=0;i<CRUISER;i++){
			mvprintw(pllr._cruiser1[i][0]*2+1,pllr._cruiser1[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>5){
		/* ship 1 */
		for(i=0;i<CRUISER;i++){
			mvprintw(pllr._cruiser2[i][0]*2+1,pllr._cruiser2[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>6){
		/* ship 1 */
		for(i=0;i<CRUISER;i++){
			mvprintw(pllr._cruiser3[i][0]*2+1,pllr._cruiser3[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>7){
		/* ship 1 */
		for(i=0;i<DESTROYER;i++){
			mvprintw(pllr._destroyer1[i][0]*2+1,pllr._destroyer1[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>8){
		/* ship 1 */
		for(i=0;i<DESTROYER;i++){
			mvprintw(pllr._destroyer2[i][0]*2+1,pllr._destroyer2[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>9){
		/* ship 1 */
		for(i=0;i<DESTROYER;i++){
			mvprintw(pllr._destroyer3[i][0]*2+1,pllr._destroyer3[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
	if(ship>10){
		/* ship 1 */
		for(i=0;i<DESTROYER;i++){
			mvprintw(pllr._destroyer4[i][0]*2+1,pllr._destroyer4[i][1]*2+1,"O");
		} /* end of for */
	} /* end of if */
} /* end of draw_boat */
	
void draw_shotted(int plr){
int i,j;
    for (i=0;i<10;i++)
        {
        for (j=0;j<10;j++)
                {
			if(pllr.shots[i][j]==1) {
				mvprintw(i*2+1,j*2+1+RIGHT_ALIGN,"x");
			}
                } /* end of for */
    } /* end of for */
}
