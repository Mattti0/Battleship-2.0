#include "header.h"

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:ask_integer
; DESCRIPTION:function ask's integer values,then accept's or declines
;	      based global constans MIN && MAX values
;	Input:
;	Output:
;  Used global variables:
; REMARKS when using this function: ncurses capable returnless ask_int
;*********************************************************************/

int ask_integer(int min , int max)
{
char value[100],trashbin;
int ok;
unsigned int incaseof=min-1;

	refresh();
	do{
		//fgets(value,100,stdin);
		getstr(value);
		ok=sscanf(value,"%d%c",&incaseof,&trashbin);
		if((incaseof < min || incaseof > max) && ok==0)
		{
		printw("Incorrect value\n");
		}
	}while(incaseof < min || incaseof > max);
	
}

/* test */