
/***************************************************************************
 *   Copyright (C) $Year$ by $Author: e1100983 $   *
 *   $Email$   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*********************************************************************
*
* 1.  NAME
*     $Source$ 
* 2.  DESCRIPTION
*
* 6.  VERSIONS
*       Original:
*         $Date: 2014-03-31 08:46:10 +0300 (Mon, 31 Mar 2014) $ / $Author: e1100983 $
*
*       Version history: Veni , Vidi , Vici
*        
*
**********************************************************************/

/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h"
/*********************************************************************
*    FUNCTIONS                                                     *
**********************************************************************/

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME: set_name
; DESCRIPTION: set player number
;	Input:
;	Output: plr (0 or 1)
;  Used global variables:
; REMARKS when using this function: 1 == 0 ; 2 == 1
;*********************************************************************/
void set_name() {
	clear();
	echo();
	int min = 1;
	int max = 2;
	int plr;
	printw("Give player number: 1 or 2\n");
	plr=ask_integer(min,max);
	if(plr==1) plr=0;
	else if(plr==2) plr=1;
	add(plr);
	
} /* end of set_name */
/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME: add()
; DESCRIPTION: loop for control boat adds
;	Input: 
;	Output:ship, size, plr to add_boat ; boat struct to filein
;  Used global variables: 
; REMARKS when using this function: 
;*********************************************************************/
void add(int plr) {
	int ship=1;
	int size;
	int i,j;
	
	for(i=0;i<10;i++){
		for(j=0;j<10;j++){
			pllr.shots[i][j]=0;
		}
	}

	for(;ship<11;ship++){
		if(ship==1) size=CARRIER;
		if(ship==2||ship==3) size=BATTLESHIP;
		if(ship>3&&ship<7) size=CRUISER;
		if(ship>6&&ship<11) size=DESTROYER;
		add_boat(plr, ship, size);
	} /* end of for */
	
	filein(plr);
	game(plr);
} /* end of add */

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME: add_boat()
; DESCRIPTION: ask boat y and x cordinate and direction for boat
;	Input: plr, ship, size
;	Output: boat cordinates to struct processing
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/
void add_boat(int plr, int ship, int size) {
	int direction; /* 0 = oikealle, 1 = alas, 2 = vasemmalle, 3 = ylös */
	int min = 0;
	int max = 9;
	int dir_min = 1;
	int dir_max = 4;
	int error = 1;
	int y,x;

	char* shiptype;
	if(ship==1) shiptype="carrier";
	if(ship==2||ship==3) shiptype="battleship";
	if(ship>3&&ship<7) shiptype="cruiser";
	if(ship>6&&ship<11) shiptype="destroyer";

	clear();
	draw_boat(ship,plr);
	draw_board();
	printw("%d. Give %s Y-axis start position (%d blocks)\n", ship, shiptype, size);
	if(ship==1) pllr._carrier[0][0]=ask_integer(min, max);
	if(ship==2) pllr._battleship1[0][0]=ask_integer(min, max);
	if(ship==3) pllr._battleship2[0][0]=ask_integer(min, max);
	if(ship==4) pllr._cruiser1[0][0]=ask_integer(min, max);
	if(ship==5) pllr._cruiser2[0][0]=ask_integer(min, max);
	if(ship==6) pllr._cruiser3[0][0]=ask_integer(min, max);
	if(ship==7) pllr._destroyer1[0][0]=ask_integer(min, max);
	if(ship==8) pllr._destroyer2[0][0]=ask_integer(min, max);
	if(ship==9) pllr._destroyer3[0][0]=ask_integer(min, max);
	if(ship==10) pllr._destroyer4[0][0]=ask_integer(min, max);
	
	printw("%d. Give %s X-axis start position (%d blocks)\n", ship, shiptype, size);
	if(ship==1) pllr._carrier[0][1]=ask_integer(min, max);
	if(ship==2) pllr._battleship1[0][1]=ask_integer(min, max);
	if(ship==3) pllr._battleship2[0][1]=ask_integer(min, max);
	if(ship==4) pllr._cruiser1[0][1]=ask_integer(min, max);
	if(ship==5) pllr._cruiser2[0][1]=ask_integer(min, max);
	if(ship==6) pllr._cruiser3[0][1]=ask_integer(min, max);
	if(ship==7) pllr._destroyer1[0][1]=ask_integer(min, max);
	if(ship==8) pllr._destroyer2[0][1]=ask_integer(min, max);
	if(ship==9) pllr._destroyer3[0][1]=ask_integer(min, max);
	if(ship==10) pllr._destroyer4[0][1]=ask_integer(min, max);
	
	printw("%d. Give %s direction (%d blocks)\n1 to right\n2 to down\n3 to left\n4 to up\n", ship, shiptype, size);
	direction = ask_integer(dir_min, dir_max);
	while(error==1) {
		if(direction==1) {
			if(ship==1 && (pllr._carrier[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==2 && (pllr._battleship1[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==3 && (pllr._battleship2[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==4 && (pllr._cruiser1[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==5 && (pllr._cruiser2[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==6 && (pllr._cruiser3[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==7 && (pllr._destroyer1[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==8 && (pllr._destroyer2[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==9 && (pllr._destroyer3[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==10 && (pllr._destroyer4[0][1]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
		} /* end of if */
		if(direction==2) {
			if(ship==1 && (pllr._carrier[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==2 && (pllr._battleship1[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==3 && (pllr._battleship2[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==4 && (pllr._cruiser1[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==5 && (pllr._cruiser2[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==6 && (pllr._cruiser3[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==7 && (pllr._destroyer1[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==8 && (pllr._destroyer2[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==9 && (pllr._destroyer3[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==10 && (pllr._destroyer4[0][0]+size) <= BOARD_SIZE) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
		} /* end of if */
		if(direction==3) {
			if(ship==1 && (pllr._carrier[0][1]+size) >= 1) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==2 && (pllr._battleship1[0][1]+size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==3 && (pllr._battleship2[0][1]+size) >= 1) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==4 && (pllr._cruiser1[0][1]+size) >= 1) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==5 && (pllr._cruiser2[0][1]+size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==6 && (pllr._cruiser3[0][1]+size) >= 1) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==7 && (pllr._destroyer1[0][1]+size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==8 && (pllr._destroyer2[0][1]+size) >= 1) {
				add_boat_struct(ship,direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==9 && (pllr._destroyer3[0][1]+size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==10 && (pllr._destroyer4[0][1]+size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
		} /* end of if */
		if(direction==4) {
			
			if(ship==1 && (pllr._carrier[0][0]-size) >= 1) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==2 && (pllr._battleship1[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==3 && (pllr._battleship2[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==4 && (pllr._cruiser1[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==5 && (pllr._cruiser2[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==6 && (pllr._cruiser3[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==7 && (pllr._destroyer1[0][0]-size) >= 1) {
				add_boat_struct(ship,  direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==8 && (pllr._destroyer2[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==9 && (pllr._destroyer3[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
			if(ship==10 && (pllr._destroyer4[0][0]-size) >= 1) {
				add_boat_struct(ship, direction, size, plr);
				error = 0;
			} /* end of if */
		} /* end of if */
		if(error==0){
			if(ship==1) pllr._carrier[0][2]=direction;
			if(ship==2) pllr._battleship1[0][2]=direction;
			if(ship==3) pllr._battleship2[0][2]=direction;
			if(ship==4) pllr._cruiser1[0][2]=direction;
			if(ship==5) pllr._cruiser2[0][2]=direction;
			if(ship==6) pllr._cruiser3[0][2]=direction;
			if(ship==7) pllr._destroyer1[0][2]=direction;
			if(ship==8) pllr._destroyer2[0][2]=direction;
			if(ship==9) pllr._destroyer3[0][2]=direction;
			if(ship==10) pllr._destroyer4[0][2]=direction;
		} /* end of if */
		if(error==1) {
			printw("%s doesn't fit!!\n Please give new direction\n", shiptype);
			direction = ask_integer(dir_min, dir_max);
		}  /* end of if */
	} /* end of while */
} /* end of add_boat */

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME: add_boat_struct
; DESCRIPTION: add boats to struct
;	Input: ship, direction, size, plr
;	Output: struct
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/
void add_boat_struct(int ship, int direction, int size, int plr){
int i=1;
	for(;i<size;i++){
		if(direction==1){
			if(ship==1) {
				pllr._carrier[i][1]=pllr._carrier[i-1][1]+1;
				pllr._carrier[i][0]=pllr._carrier[i-1][0];
				pllr._carrier[i][2]=direction;
			} /* end of if */
			if(ship==2) {
				pllr._battleship1[i][1]=pllr._battleship1[i-1][1]+1;
				pllr._battleship1[i][0]=pllr._battleship1[i-1][0];
				pllr._battleship1[i][2]=direction;
			} /* end of if */
			if(ship==3) {
				pllr._battleship2[i][1]=pllr._battleship2[i-1][1]+1;
				pllr._battleship2[i][0]=pllr._battleship2[i-1][0];
				pllr._battleship2[i][2]=direction;
			} /* end of if */
			if(ship==4) {
				pllr._cruiser1[i][1]=pllr._cruiser1[i-1][1]+1;
				pllr._cruiser1[i][0]=pllr._cruiser1[i-1][0];
				pllr._cruiser1[i][2]=direction;
			} /* end of if */
			if(ship==5) {
				pllr._cruiser2[i][1]=pllr._cruiser2[i-1][1]+1;
				pllr._cruiser2[i][0]=pllr._cruiser2[i-1][0];
				pllr._cruiser2[i][2]=direction;
			} /* end of if */
			if(ship==6) {
				pllr._cruiser3[i][1]=pllr._cruiser3[i-1][1]+1;
				pllr._cruiser3[i][0]=pllr._cruiser3[i-1][0];
				pllr._cruiser3[i][2]=direction;
			} /* end of if */
			if(ship==7) {
				pllr._destroyer1[i][1]=pllr._destroyer1[i-1][1]+1;
				pllr._destroyer1[i][0]=pllr._destroyer1[i-1][0];
				pllr._destroyer1[i][2]=direction;
			} /* end of if */
			if(ship==8) {
				pllr._destroyer2[i][1]=pllr._destroyer2[i-1][1]+1;
				pllr._destroyer2[i][0]=pllr._destroyer2[i-1][0];
				pllr._destroyer2[i][2]=direction;
			} /* end of if */
			if(ship==9) {
				pllr._destroyer3[i][1]=pllr._destroyer3[i-1][1]+1;
				pllr._destroyer3[i][0]=pllr._destroyer3[i-1][0];
				pllr._destroyer3[i][2]=direction;
			} /* end of if */
			if(ship==10) {
				pllr._destroyer4[i][1]=pllr._destroyer4[i-1][1]+1;
				pllr._destroyer4[i][0]=pllr._destroyer4[i-1][0];
				pllr._destroyer4[i][2]=direction;
			} /* end of if */
		} /* end of if */
		if(direction==2){
			if(ship==1) {
				pllr._carrier[i][0]=pllr._carrier[i-1][0]+1;
				pllr._carrier[i][1]=pllr._carrier[i-1][1];
				pllr._carrier[i][2]=direction;
			} /* end of if */
			if(ship==2) {
				pllr._battleship1[i][0]=pllr._battleship1[i-1][0]+1;
				pllr._battleship1[i][1]=pllr._battleship1[i-1][1];
				pllr._battleship1[i][2]=direction;
			} /* end of if */
			if(ship==3) {
				pllr._battleship2[i][0]=pllr._battleship2[i-1][0]+1;
				pllr._battleship2[i][1]=pllr._battleship2[i-1][1];
				pllr._battleship2[i][2]=direction;
			} /* end of if */
			if(ship==4) {
				pllr._cruiser1[i][0]=pllr._cruiser1[i-1][0]+1;
				pllr._cruiser1[i][1]=pllr._cruiser1[i-1][1];
				pllr._cruiser1[i][2]=direction;
			} /* end of if */
			if(ship==5) {
				pllr._cruiser2[i][0]=pllr._cruiser2[i-1][0]+1;
				pllr._cruiser2[i][1]=pllr._cruiser2[i-1][1];
				pllr._cruiser2[i][2]=direction;
			} /* end of if */
			if(ship==6) {
				pllr._cruiser3[i][0]=pllr._cruiser3[i-1][0]+1;
				pllr._cruiser3[i][1]=pllr._cruiser3[i-1][1];
				pllr._cruiser3[i][2]=direction;
			} /* end of if */
			if(ship==7) {
				pllr._destroyer1[i][0]=pllr._destroyer1[i-1][0]+1;
				pllr._destroyer1[i][1]=pllr._destroyer1[i-1][1];
				pllr._destroyer1[i][2]=direction;
			} /* end of if */
			if(ship==8) {
				pllr._destroyer2[i][0]=pllr._destroyer2[i-1][0]+1;
				pllr._destroyer2[i][1]=pllr._destroyer2[i-1][1];
				pllr._destroyer2[i][2]=direction;
			} /* end of if */
			if(ship==9) {
				pllr._destroyer3[i][0]=pllr._destroyer3[i-1][0]+1;
				pllr._destroyer3[i][1]=pllr._destroyer3[i-1][1];
				pllr._destroyer3[i][2]=direction;
			} /* end of if */
			if(ship==10) {
				pllr._destroyer4[i][0]=pllr._destroyer4[i-1][0]+1;
				pllr._destroyer4[i][1]=pllr._destroyer4[i-1][1];
				pllr._destroyer4[i][2]=direction;
			} /* end of if */
		} /* end of if */
		if(direction==3){
			if(ship==1) {
				pllr._carrier[i][1]=pllr._carrier[i-1][1]-1;
				pllr._carrier[i][0]=pllr._carrier[i-1][0];
				pllr._carrier[i][2]=direction;
			} /* end of if */
			if(ship==2) {
				pllr._battleship1[i][1]=pllr._battleship1[i-1][1]-1;
				pllr._battleship1[i][0]=pllr._battleship1[i-1][0];
				pllr._battleship1[i][2]=direction;
			} /* end of if */
			if(ship==3) {
				pllr._battleship2[i][1]=pllr._battleship2[i-1][1]-1;
				pllr._battleship2[i][0]=pllr._battleship2[i-1][0];
				pllr._battleship2[i][2]=direction;
			} /* end of if */
			if(ship==4) {
				pllr._cruiser1[i][1]=pllr._cruiser1[i-1][1]-1;
				pllr._cruiser1[i][2]=direction;
				pllr._cruiser1[i][0]=pllr._cruiser1[i-1][0];
			} /* end of if */
			if(ship==5) {
				pllr._cruiser2[i][1]=pllr._cruiser2[i-1][1]-1;
				pllr._cruiser2[i][0]=pllr._cruiser2[i-1][0];
				pllr._cruiser2[i][2]=direction;
			} /* end of if */
			if(ship==6) {
				pllr._cruiser3[i][1]=pllr._cruiser3[i-1][1]-1;
				pllr._cruiser3[i][0]=pllr._cruiser3[i-1][0];
				pllr._cruiser3[i][2]=direction;
			} /* end of if */
			if(ship==7) {
				pllr._destroyer1[i][1]=pllr._destroyer1[i-1][1]-1;
				pllr._destroyer1[i][0]=pllr._destroyer1[i-1][0];
				pllr._destroyer1[i][2]=direction;
			} /* end of if */
			if(ship==8) {
				pllr._destroyer2[i][1]=pllr._destroyer2[i-1][1]-1;
				pllr._destroyer2[i][0]=pllr._destroyer2[i-1][0];
				pllr._destroyer2[i][2]=direction;
			} /* end of if */
			if(ship==9) {
				pllr._destroyer3[i][1]=pllr._destroyer3[i-1][1]-1;
				pllr._destroyer3[i][0]=pllr._destroyer3[i-1][0];
				pllr._destroyer3[i][2]=direction;
			} /* end of if */
			if(ship==10) {
				pllr._destroyer4[i][1]=pllr._destroyer4[i-1][1]-1;
				pllr._destroyer4[i][0]=pllr._destroyer4[i-1][0];
				pllr._destroyer4[i][2]=direction;
			} /* end of if */
		} /* end of if */
		if(direction==4){
			if(ship==1) {
				pllr._carrier[i][0]=pllr._carrier[i-1][0]-1;
				pllr._carrier[i][1]=pllr._carrier[i-1][1];
				pllr._carrier[i][2]=direction;
			} /* end of if */
			if(ship==2) {
				pllr._battleship1[i][0]=pllr._battleship1[i-1][0]-1;
				pllr._battleship1[i][1]=pllr._battleship1[i-1][1];
				pllr._battleship1[i][2]=direction;
			} /* end of if */
			if(ship==3) {
				pllr._battleship2[i][0]=pllr._battleship2[i-1][0]-1;
				pllr._battleship2[i][1]=pllr._battleship2[i-1][1];
				pllr._battleship2[i][2]=direction;
			} /* end of if */
			if(ship==4) {
				pllr._cruiser1[i][0]=pllr._cruiser1[i-1][0]-1;
				pllr._cruiser1[i][1]=pllr._cruiser1[i-1][1];
				pllr._cruiser1[i][2]=direction;
			} /* end of if */
			if(ship==5) {
				pllr._cruiser2[i][0]=pllr._cruiser2[i-1][0]-1;
				pllr._cruiser2[i][1]=pllr._cruiser2[i-1][1];
				pllr._cruiser2[i][2]=direction;
			} /* end of if */
			if(ship==6) {
				pllr._cruiser3[i][0]=pllr._cruiser3[i-1][0]-1;
				pllr._cruiser3[i][1]=pllr._cruiser3[i-1][1];
				pllr._cruiser3[i][2]=direction;
			} /* end of if */
			if(ship==7) {
				pllr._destroyer1[i][0]=pllr._destroyer1[i-1][0]-1;
				pllr._destroyer1[i][1]=pllr._destroyer1[i-1][1];
				pllr._destroyer1[i][2]=direction;
			} /* end of if */
			if(ship==8) {
				pllr._destroyer2[i][0]=pllr._destroyer2[i-1][0]-1;
				pllr._destroyer2[i][1]=pllr._destroyer2[i-1][1];
				pllr._destroyer2[i][2]=direction;
			}
			if(ship==9) {
				pllr._destroyer3[i][0]=pllr._destroyer3[i-1][0]-1;
				pllr._destroyer3[i][1]=pllr._destroyer3[i-1][1];
				pllr._destroyer3[i][2]=direction;
			} /* end of if */
			if(ship==10) {
				pllr._destroyer4[i][0]=pllr._destroyer4[i-1][0]-1;
				pllr._destroyer4[i][1]=pllr._destroyer4[i-1][1];
				pllr._destroyer4[i][2]=direction;
			} /* end of if */
		} /* end of if */
	} /* end of for */
} /* end of add_boat_struct */
