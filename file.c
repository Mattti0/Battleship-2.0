/*-------------------------------------------------------------------*
*    HEADER FILES                                                    *
*--------------------------------------------------------------------*/
#include "header.h" 

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:	filein
; DESCRIPTION:save values from struct to file*plr*.txt
;	Input: int plr
;	Output: file.txt
;  Used global variables: none
; REMARKS when using this function: pre alpha
;*********************************************************************/

void filein(int plr)	{
FILE	*save;

if(plr == 0)	{
	save = fopen("file1.txt","w");
}
else		{
	save = fopen("file2.txt","w");
}

for(int i=0; i<4 ; i++)	{
	fprintf	(save,"%d %d %d %d %d %d %d %d %d %d\n",
        pllr._carrier[CARRIER][i],
        pllr._battleship1[BATTLESHIP][i],
        pllr._battleship2[BATTLESHIP][i],
        pllr._cruiser1[CRUISER][i],
        pllr._cruiser2[CRUISER][i],
        pllr._cruiser3[CRUISER][i],
        pllr._destroyer1[DESTROYER][i],
        pllr._destroyer2[DESTROYER][i],
        pllr._destroyer3[DESTROYER][i],
        pllr._destroyer4[DESTROYER][i]
	);
}
fclose(save);
}

/*********************************************************************
;	F U N C T I O N    D E S C R I P T I O N
;---------------------------------------------------------------------
; NAME:	fileout
; DESCRIPTION:reads the strings from file*plr* and puts the into the player struct
;	Input:plr 1 = 0 , 2 = 1 
;	Output:
;  Used global variables:
; REMARKS when using this function:
;*********************************************************************/

void fileout(int plr)	{
FILE	*open;
char buffer[100];
int j,i;

if(plr == 0)		{
	open = fopen("file2.txt","r");
	j = 1;
}
else			{
	open = fopen("file1.txt","r");
	j = 0;
}

for(i=0; i<4; i++)	{
	fgets(buffer, 100 , open);
	sscanf	(buffer,"%d %d %d %d %d %d %d %d %d %d %d",
        &player[j]._carrier[CARRIER][i],
        &player[j]._battleship1[BATTLESHIP][i],
        &player[j]._battleship2[BATTLESHIP][i],
        &player[j]._cruiser1[CRUISER][i],
        &player[j]._cruiser2[CRUISER][i],
        &player[j]._cruiser3[CRUISER][i],
        &player[j]._destroyer1[DESTROYER][i],
        &player[j]._destroyer2[DESTROYER][i],
        &player[j]._destroyer3[DESTROYER][i],
        &player[j]._destroyer4[DESTROYER][i]
	);

}
fclose(open);
}/* end of fileout */


void filecheck(int plr) {
int cont = 1;
FILE *check;
do{
	if (plr==0){
		if(check = fopen("file2.txt","r")){
		fclose(check);
		cont = 0;
		}
	}
	if (plr==1){
		if(check = fopen("file1.txt","r")){
		fclose(check);
		cont = 0;
		}
	}
}while(cont!=0);
cont = 0;
}
